#
# Copyright (C) 2018  Sage Vaillancourt, sagev9000@gmail.com
#
# This file is part of Fronter.
# 
# Fronter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Fronter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Fronter.  If not, see <http://www.gnu.org/licenses/>.
#

extends Node

signal multiplayer_menu
signal start_game

var clockwise = false
var rotation_multiplier = 1
const PEAK_ANGLE = -15
const TROUGH_ANGLE = -5
const SLOW_ROTATION = 0.8
const REGULAR_ROTATION = 1
var now_quitting = false
var rectangle_opacity = 0

func _ready():
	$Music.play()
	pass

func _process(delta):
	if now_quitting == true:
		$StartEndlessMode.visible = false
		$ColorRect.visible = true
		$ColorRect.color = Color(0, 0, 0, rectangle_opacity)
#	if rectangle_opacity < 0.75:
		rectangle_opacity += delta/2
		$Music.volume_db -= delta*20
#	else:
#		rectangle_opacity += delta/4
		if rectangle_opacity >= 1:
			get_tree().paused = false
			queue_free()
	
	if (Input.is_action_pressed("ui_quit")):
		get_tree().quit()
	
	#if (Input.is_action_pressed("ui_accept")):
	#	now_quitting = true
	
	if ($Ship.rotation_degrees > -7 || $Ship.rotation_degrees < -13):
		rotation_multiplier = SLOW_ROTATION
	else:
		rotation_multiplier = REGULAR_ROTATION
	
	if clockwise == true:
		$Ship.rotation_degrees += delta
		if $Ship.rotation_degrees > TROUGH_ANGLE:
			clockwise = false
	else:
		$Ship.rotation_degrees -= delta
		if $Ship.rotation_degrees < PEAK_ANGLE:
			clockwise = true
		
	#if Input.is_action_pressed("ui_accept"):
	#	_on_Button_pressed()

func _on_Button_pressed():
	#$Button.visible = false
	emit_signal("start_game", 1)
	now_quitting = true

func _on_Multiplayer_pressed():
	emit_signal("multiplayer_menu")

func _on_Settings_pressed():
	var settings = preload("res://Menus/Settings.tscn").instance()
	add_child(settings)