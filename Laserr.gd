extends Area2D

signal hit

export (int) var SPEED
export (int) var DAMAGE
var plasma = false

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _on_Visibility_screen_exited():
    queue_free()

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	$AnimatedSprite.play()
	connect("area_entered", self, "hit")
	pass

func hit(who): 
	if plasma == false:
		hide()
		queue_free()

func _process(delta):
	var velocity = Vector2()
	velocity.x += 1
	velocity = velocity.normalized() * SPEED
	position += velocity * delta

func _on_Laser_body_entered(body):
	emit_signal("hit")
	if plasma == false:
		$CollisionShape2D.disabled = true
		queue_free()
	else:
		var timer = null
		timer = Timer.new()
		timer.set_one_shot(true)
		timer.set_wait_time(0.1)
		timer.connect("timeout", self, "on_timeout_complete")
		add_child(timer)
		$CollisionShape2D.disabled = true
		
func on_timeout_complete():
	$CollisionShape2D.disabled = false