extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _on_BackButton_pressed():
	queue_free()

func _on_PatreonLink_pressed():
	OS.shell_open("https://www.patreon.com/fronter")

func _on_LiberaLink_pressed():
	OS.shell_open("https://www.liberapay.com/fronter")
