#
# Copyright (C) 2018  Sage Vaillancourt, sagev9000@gmail.com
#
# This file is part of Fronter.
# 
# Fronter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Fronter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Fronter.  If not, see <http://www.gnu.org/licenses/>.
#

extends Area2D


signal update_display # tells parent to update points display
signal refund # will tell parent a refund is occuring, for display
signal shooting_speed_upgrade # signals that a shooting speed upgrade has occured
signal ship_speed_upgrade 
signal shooting_speed_fully_upgraded
signal ship_speed_fully_upgraded
signal restart_game
signal multiplayer_movement

export (PackedScene) var Laser

################################
#THINGS THAT MAY NEED ADJUSTING
#################################
#Default ship strengths and costs
const BULLET_DELAY_TIER1 = 0.8
const BULLET_DELAY_TIER2 = 0.5
const BULLET_DELAY_TIER3 = 0.2
const BULLET_DELAY_TIER4 = 0.1
const BULLET_DELAY_TIER5 = 0.05
const SHOOTING_SPEED_UPGRADE_DEFAULT = 1500
const BULLET_DELAY_MINIMUM = 0.01
const BULLET_DELAY_TIER2_COST = 100
const BULLET_DELAY_TIER3_COST = 200
const BULLET_DELAY_TIER4_COST = 400
const BULLET_DELAY_TIER5_COST = 1000

const SHIP_SPEED_TIER1 = 150
const SHIP_SPEED_TIER2 = 200
const SHIP_SPEED_TIER3 = 300
const SHIP_SPEED_TIER4 = 500
const SHIP_SPEED_TIER5 = 800
const SHIP_SPEED_UPGRADE_DEFAULT = 1500
const SHIP_SPEED_MAXIMUM = 1200
const SHIP_SPEED_TIER2_COST = 200
const SHIP_SPEED_TIER3_COST = 400
const SHIP_SPEED_TIER4_COST = 600
const SHIP_SPEED_TIER5_COST = 800

const STARTING_MONEY = 100

#################################

var screensize

var ship_speed = SHIP_SPEED_TIER1
var ship_speed_tier = 0

var bullet_delay = BULLET_DELAY_TIER1
var bullet_delay_tier = 0
var laser_penetration = 0
var double_laser = false

var ship_value = 0
var refund_percentage = 1
var money = STARTING_MONEY

var shooting_speed_upgrade = SHOOTING_SPEED_UPGRADE_DEFAULT
var ship_speed_upgrade = SHIP_SPEED_UPGRADE_DEFAULT

func upgradeMenu():
	var upgmenu = preload("res://UpgradeMenu.tscn").instance()
	add_child(upgmenu)
	upgmenu.connect("refund", self, "_refund_button")
	upgmenu.connect("bullet_delay_upgrade", self, "upgradeBulletDelay_button")
	upgmenu.connect("ship_speed_upgrade", self, "upgradeShipSpeed_button")
	upgmenu.bullet_delay_tier = bullet_delay_tier
	upgmenu.ship_speed_tier = ship_speed_tier
	get_tree().paused = true

func upgradeShipSpeed_button():
	upgradeShipSpeed()
	emit_signal("update_display")
	get_tree().paused = true

func upgradeBulletDelay_button():
	upgradeBulletDelay()
	emit_signal("update_display")
	get_tree().paused = true

func upgradeBulletDelay():
	if (bullet_delay == BULLET_DELAY_TIER1 && money >= BULLET_DELAY_TIER2_COST):
		bullet_delay = BULLET_DELAY_TIER2
		money -= BULLET_DELAY_TIER2_COST
		ship_value += BULLET_DELAY_TIER2_COST
		bullet_delay_tier = 1
	elif (bullet_delay == BULLET_DELAY_TIER2 && money >= BULLET_DELAY_TIER3_COST):
		bullet_delay = BULLET_DELAY_TIER3
		money -= BULLET_DELAY_TIER3_COST
		ship_value += BULLET_DELAY_TIER3_COST
		bullet_delay_tier = 2
	elif (bullet_delay == BULLET_DELAY_TIER3 && money >= BULLET_DELAY_TIER4_COST):
		bullet_delay = BULLET_DELAY_TIER4
		money -= BULLET_DELAY_TIER4_COST
		ship_value += BULLET_DELAY_TIER4_COST
		bullet_delay_tier = 3
	elif (bullet_delay == BULLET_DELAY_TIER4 && money >= BULLET_DELAY_TIER5_COST):
		bullet_delay = BULLET_DELAY_TIER5
		money -= BULLET_DELAY_TIER5_COST
		ship_value += BULLET_DELAY_TIER5_COST
		bullet_delay_tier = 4
	elif (bullet_delay <= BULLET_DELAY_MINIMUM):
		emit_signal("bullet_delay_fully_upgraded")
	elif (bullet_delay <= BULLET_DELAY_TIER5 && money >= shooting_speed_upgrade):
		bullet_delay = bullet_delay*0.95
		money -= shooting_speed_upgrade
		ship_value += shooting_speed_upgrade
		shooting_speed_upgrade *= 1.1
		bullet_delay_tier += 1
	timer.set_wait_time(bullet_delay)
	prints(bullet_delay)

func upgradeShipSpeed():
	prints("func upgradeShipSpeed():")
	if (ship_speed == SHIP_SPEED_TIER1 && money >= SHIP_SPEED_TIER2_COST):
		ship_speed = SHIP_SPEED_TIER2
		money -= SHIP_SPEED_TIER2_COST
		ship_value += SHIP_SPEED_TIER2_COST
		ship_speed_tier = 1
	elif (ship_speed == SHIP_SPEED_TIER2 && money >= SHIP_SPEED_TIER3_COST):
		ship_speed = SHIP_SPEED_TIER3
		money -= SHIP_SPEED_TIER3_COST
		ship_value += SHIP_SPEED_TIER3_COST
		ship_speed_tier = 2
	elif (ship_speed == SHIP_SPEED_TIER3 && money >= SHIP_SPEED_TIER4_COST):
		ship_speed = SHIP_SPEED_TIER4
		money -= SHIP_SPEED_TIER4_COST
		ship_value += SHIP_SPEED_TIER4_COST
		ship_speed_tier = 3
	elif (ship_speed == SHIP_SPEED_TIER4 && money >= SHIP_SPEED_TIER5_COST):
		ship_speed = SHIP_SPEED_TIER5
		money -= SHIP_SPEED_TIER5_COST
		ship_value += SHIP_SPEED_TIER5_COST
		ship_speed_tier = 4
	elif (ship_speed >= SHIP_SPEED_MAXIMUM):
		emit_signal("ship_speed_fully_upgraded")
	elif (ship_speed >= SHIP_SPEED_TIER5 && money >= ship_speed_upgrade):
		ship_speed = ship_speed*1.05
		money -= ship_speed_upgrade
		ship_value += ship_speed_upgrade
		ship_speed_upgrade *= 1.1
	prints(ship_speed)

func _ready():
	timer = Timer.new()
	timer.set_one_shot(true)
	timer.set_wait_time(bullet_delay)
	timer.connect("timeout", self, "on_timeout_complete")
	add_child(timer)
	timer.start()
	
	position.x = 100
	position.y = 250
	
	screensize = get_viewport_rect().size
	$AnimatedSprite.play()

func _refund_button():
	bullet_delay = BULLET_DELAY_TIER1
	bullet_delay_tier = 0
	timer.set_wait_time(bullet_delay)
	ship_speed = SHIP_SPEED_TIER1
	shooting_speed_upgrade = SHOOTING_SPEED_UPGRADE_DEFAULT
	money += ship_value*refund_percentage
	prints("Refunded ", ship_value*refund_percentage)
	ship_value = 0
	emit_signal("update_display")
	get_tree().paused = true

var timer = null

var can_shoot = false
var shoot_down = false

func on_timeout_complete():
	can_shoot = true

#func _unhandled_input(event):
#	if (event is InputEventScreenTouch):
#		gravity_point = true
#		gravity_vec = Vector2(0.5, 0.5)

var x_up = false
var y_up = false
var x_down = false
var y_down = false

func moveto(finger_position):
	if (finger_position.x > position.x && finger_position.x < 800):
		velocity.x += 1
		x_up = true
	if position.x > finger_position.x:
		velocity.x -= 1
		x_down = true
	if (finger_position.y - 100 > position.y && finger_position.x < 800):
		velocity.y += 1
		y_up = true
	if (position.y > finger_position.y - 100 && finger_position.x < 800):
		velocity.y -= 1
		y_down = true
#	if (finger_position.x >= 800 && can_shoot):
#		shoot()
	if get_tree().has_network_peer():
		rpc("move_player", get_tree().get_network_unique_id(), position)

remote func move_player(id, position):
	prints("PLAYER SAYS:", id, position)
	emit_signal("multiplayer_movement", id, position)

var velocity = Vector2() # the player's movement vector

func _process(delta):
	velocity = Vector2()
	if (Input.is_action_pressed("ui_right") || x_up):
		velocity.x += 1
		x_up = false
	if (Input.is_action_pressed("ui_left") || x_down):
		velocity.x -= 1
		x_down = false
	if (Input.is_action_pressed("ui_down") || y_up):
		velocity.y += 1
		y_up = false
	if (Input.is_action_pressed("ui_up") || y_down):
		velocity.y -= 1
		y_down = false
	if (velocity.length() > 0):
		velocity = velocity.normalized() * ship_speed
	
	position += velocity * delta
	position.x = clamp(position.x, 0, screensize.x)
	position.y = clamp(position.y, 0, screensize.y)
	if get_tree().has_network_peer():
		rpc("move_player", get_tree().get_network_unique_id(), position)
	
	if ((Input.is_action_pressed("ui_accept") || shoot_down == true) && can_shoot == true):
		shoot()

	if (Input.is_action_pressed("ui_reset") && can_shoot == true):
		upgradeMenu()
	
	var x_up = false
	var y_up = false
	var x_down = false
	var y_down = false

func _on_Player_body_entered(body):
	prints("hit")

func shootDown():
	shoot_down = true
func shootUp():
	shoot_down = false

func shoot():
	var laser = Laser.instance()
	get_node("../").add_child(laser)
	laser.current_pen = laser_penetration
	laser.position.y = position.y - 27
	laser.position.x = position.x + 46
	
	#MAYBE THE LASERS SHOULD BE THEIR OWN NODES
	#Would allow for more simple additions in the future
	if double_laser == true:
		var laser2 = Laser.instance()
		get_node("../").add_child(laser2)
		laser2.position.y = position.y + 28
		laser2.position.x = position.x + 46
		laser2.current_pen = laser_penetration
		
	
	can_shoot = false
	timer.start()
	
func gameOver():
	var gameover = preload("res://GameOver.tscn").instance()
	add_child(gameover)
	gameover.connect("restart", self, "restart_game")
#	gameover.connect("bullet_delay_upgrade", self, "upgradeBulletDelay_button")
	get_tree().paused = true

func restart_game():
	_refund_button()
	get_tree().paused = false
	money = STARTING_MONEY
	emit_signal("restart_game")