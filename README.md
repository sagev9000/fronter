Fronter is side-scrolling shooter game by sagethesagesage.

Fronter is very much in an alpha state, and many things need improvement, but it is quite playable, and supports LAN-based multiplayer.

Donations are accepted at patreon.com/fronter and liberapay.com/fronter, and the intention is to accept Ethereum at some point, as well. 
