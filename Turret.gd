extends Node2D

signal upgrade_turret

var can_shoot = true
var damage = 15
var reload_time = 1.5

var current_bad_id
var current_bad = null
var current_bad_shape

var tier = 1

const RANGE_TIER = {1:Vector2(30,30), 2:Vector2(40,40), 3:Vector2(50,50), 4:Vector2(60,60)}
const DAMAGE_TIER = {1:15, 2:25, 3:50, 4:100}
const RELOAD_TIER  = {1:1.5, 2:1, 3:0.75, 4:0.5}
const ANIMATION_TIER = {1:"plain", 2:"point", 3:"spike", 4:"spike"}
const ANIMATION_SCALE_TIER = {1:Vector2(.12,.12), 2:Vector2(.2,.2), 3:Vector2(.2,.2), 4:Vector2(.2,.2)}

var enemy_count = 0

export (PackedScene) var UMenu

func _ready():
	$ReloadTimer.wait_time = RELOAD_TIER[tier]
	damage = DAMAGE_TIER[tier]
	$Range.scale = RANGE_TIER[tier]
	$TurretSprite.animation = ANIMATION_TIER[tier]
	$TurretSprite.scale = ANIMATION_SCALE_TIER[tier]
	$Damage.text = str(damage)
	pass

var zeroPoint = Vector2(-60, -1015)

func _process(delta):
	if current_bad:
		var wr = weakref(current_bad)
		if wr.get_ref():
			if current_bad != null:
				var local_pos = Vector2()
				local_pos = to_local(current_bad.position)
				$Beam.set_point_position(1, local_pos)
				$TurretSprite.rotation = ($Beam.get_point_position(0).angle_to($Beam.get_point_position(1))) + PI/5.3
			
			if ("Bad" in current_bad.get_name()) && can_shoot:
				$Beam.default_color.a = 200
				$TurretSprite.frame = 1
				if current_bad.health > damage: 
					current_bad.hit(self)
				else: 
					current_bad.hit(self)
					current_bad = null
				$FlashTimer.start()
				can_shoot = false

func entityEnteredRange(bad_id, bad, bad_shape, self_shape):
	#print(bad.get_name())
	if ("Bad" in bad.get_name() || "Black" in bad.get_name() || "Boss" in bad.get_name()) || "Prison" in bad.get_name():# && current_bad == null:
		current_bad = bad
		current_bad_id = bad_id
		current_bad_shape = bad_shape
		enemy_count += 1
		#print(enemy_count)

func _on_FlashTimer_timeout():
	$Beam.default_color.a = 0
	$TurretSprite.frame = 0
	pass # replace with function body

func _on_ReloadTimer_timeout():
	can_shoot = true
	pass # replace with function body

func entityExitedRange(bad_id, bad, bad_shape, self_shape):
	if weakref(bad):
		if ("Bad" in bad.get_name()):
			enemy_count -= 1
	
	if bad_id == current_bad_id:
		current_bad = null
		current_bad_id = null
		current_bad_shape = null

func _on_OpenMenuButton_pressed():
	var menu = UMenu.instance()
	add_child(menu)
	menu.connect("turret_upgrade", self, "upgradeTurret")
	pass

func upgradeTurret():
	tier += 1
	$ReloadTimer.wait_time = RELOAD_TIER[tier]
	damage = DAMAGE_TIER[tier]
	$Range.scale = RANGE_TIER[tier]
	$TurretSprite.animation = ANIMATION_TIER[tier]
	$TurretSprite.scale = ANIMATION_SCALE_TIER[tier]

func relayUpgrade():
	print("relayUpgrade()")
	emit_signal("upgrade_turret", position)