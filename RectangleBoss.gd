#
# Copyright (C) 2018  Sage Vaillancourt, sagev9000@gmail.com
#
# This file is part of Fronter.
# 
# Fronter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Fronter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Fronter.  If not, see <http://www.gnu.org/licenses/>.
#

extends Area2D

signal dead
signal flash
signal boss_fight_over
signal boss_health
export (PackedScene) var Laser

#### MAYBE ADD A HEALTH BAR TO MAIN ITSELF ####
#var health = 3000
# DEFAULT: 
var starting_health = 3000
var health = starting_health

var timer = null
var hit_timer = 1000
var health_multi = 1
var move_down = true
var small_move_down = true
var speed_multiplier = 3
var flashed = false

func _ready():
	connect("area_entered", self, "hit")
	timer = Timer.new()
	timer.set_one_shot(false)
	timer.set_wait_time(3)
	timer.connect("timeout", self, "shoot")
	add_child(timer)
	timer.start()
	pass

func hit(who):
	health -= who.damage/(health_multi*3.5)
	if health > 1500:
		$Inside/InsideBadSprite.frame = 1
		hit_timer = 0
	else:
		$Inside/InsideBadSprite.frame = 2
	updateOutsideSprite()
	if get_tree().is_network_server():
			rpc("bossHealth", health)
	emit_signal("boss_health", health, starting_health)

func _process(delta):
	# Entering view
	if position.x > 0:
		position.x -= delta*((100+(position.x))/10)
	
	wobble(delta)
	
	if health < -1*(starting_health/10) && $Inside/InsideBadSprite.rotation_degrees < 180:
		$Inside/InsideBadSprite.rotation_degrees += delta*20
	
	if $Inside/InsideBadSprite.rotation_degrees > 179 && $Inside/InsideBadSprite.rotation_degrees < 190:
		if position.x > 40:
			$Inside/InsideBadSprite.frame = 4
		position.x += delta*75
	
	if health <= 0 && $BigBadSprite.position.x < 5000:
			$BigBadSprite.position.x += delta*75
	
	if position.x > 295 && health <= 0:
		emit_signal("dead", 1000)
		emit_signal("boss_fight_over")
		queue_free()
	
	#### SMALL WOBBLE ####
	if $Inside/InsideBadSprite.position.y > 1200:
		small_move_down = false
	if $Inside/InsideBadSprite.position.y <= 1182:
		small_move_down = true
		
	if small_move_down:
		$Inside/InsideBadSprite.position.y += delta*6
	else:
		$Inside/InsideBadSprite.position.y -= delta*6
	
	#### DOES LOW HEALTH RELEASE THE INSIDE OR NO HEALTH? ####
	if health <= 0 && !flashed:
		emit_signal("flash")
		$OutsideBottomCollision.disabled = true
		$OutsideTopCollision.disabled = true
		flashed = true
		#queue_free()
	
	if hit_timer < 0.15:
		hit_timer += delta
	elif hit_timer < 0.25:
		hit_timer += delta
	else:
		if health > 1500:
			$Inside/InsideBadSprite.frame = 0
	
	var health_bar = Vector2(((health * 6) - 157), -273)
	#$Line2D.set_point_position( 1, health_bar )

func _on_Inside_area_entered(area):
	health -= 15/health_multi
	updateOutsideSprite()
	if health > 1500:
		$Inside/InsideBadSprite.frame = 1
		hit_timer = 0
	else:
		$Inside/InsideBadSprite.frame = 2
	if get_tree().is_network_server():
		rpc("bossHealth", health)
	else:
		pass
	emit_signal("boss_health", health, starting_health)
	
func updateOutsideSprite():
	if health > starting_health*.9:
		$BigBadSprite.frame = 0
	elif health > starting_health*.8:
		$BigBadSprite.frame = 1
	elif health > starting_health*.7:
		$BigBadSprite.frame = 2
	elif health > starting_health*.6:
		$BigBadSprite.frame = 3
	elif health > starting_health*.5:
		$BigBadSprite.frame = 4
	elif health > starting_health*.45:
		$BigBadSprite.frame = 5
	elif health > starting_health*.4:
		$BigBadSprite.frame = 6
	elif health > starting_health*.35:
		$BigBadSprite.frame = 7
	elif health > starting_health*.3:
		$BigBadSprite.frame = 8
	elif health > starting_health*.25:
		$BigBadSprite.frame = 9
	elif health > starting_health*.2:
		$BigBadSprite.frame = 10
	elif health > starting_health*.15:
		$BigBadSprite.frame = 11
	elif health > starting_health*.1:
		$BigBadSprite.frame = 12
	elif health > starting_health*.075:
		timer.stop()
		$BigBadSprite.frame = 13
		$Inside/InsideBadSprite.frame = 2
	elif health <= 0:
		$BigBadSprite.frame = 14

slave func bossHealth(host_health):
	health = host_health

func wobble(delta):
	if position.y > 4:
		move_down = false
	if position.y <= -4:
		move_down = true
	
	if abs(position.y) > 4:
		speed_multiplier = 1
	elif abs(position.y) > 3:
		speed_multiplier = 1
	elif abs(position.y) > 2:
		speed_multiplier = 2
	elif abs(position.y) <= 2:
		speed_multiplier = 3
	
	if move_down:
		if health > 1000:
			position.y += delta*speed_multiplier
	else:
		if health > 1000:
			position.y -= delta*speed_multiplier

var top_laser = true
func shoot():
	var laser = Laser.instance()
	get_node("../").add_child(laser)
	laser.friendly_laser = false
	laser.position.x = position.x + 720
	laser.rotation_degrees = 180
	if top_laser:
		laser.position.y = position.y + 110
		top_laser = false
	else:
		laser.position.y = position.y + 470
		top_laser = true