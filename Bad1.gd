#
# Copyright (C) 2018  Sage Vaillancourt, sagev9000@gmail.com
#
# This file is part of Fronter.
# 
# Fronter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Fronter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Fronter.  If not, see <http://www.gnu.org/licenses/>.
#

extends Area2D

signal dead
export (int) var SPEED
export (PackedScene) var Laser
var screensize
var health_multi = 1

var boss = false
var health = 50
var hit_timer = 1000

func _ready():
	connect("area_entered", self, "hit")
	pass

func hit(who):
	health -= who.damage/health_multi
	$AnimatedSprite.frame = 1
	hit_timer = 0
	
	var health_bar = Vector2(((health * 6) - 157), -273)
	$Line2D.set_point_position( 1, health_bar )
	
	if health <= 0:
		emit_signal("dead", 10) # was 12 # was 15
		queue_free()

var velocity = Vector2()

func _process(delta):
	if hit_timer < 0.15:
		hit_timer += delta
		velocity.x = 0
	elif hit_timer < 0.25:
		hit_timer += delta
		velocity.x -= 1
	else:
		velocity.x -= 1
		$AnimatedSprite.frame = 0
	
	if velocity.length() > 0:
		velocity = velocity.normalized() * SPEED
	
	position += velocity * delta
	
	if position.x < -100:
		emit_signal("dead", 0)
		queue_free()