#
# Copyright (C) 2018  Sage Vaillancourt, sagev9000@gmail.com
#
# This file is part of Fronter.
# 
# Fronter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Fronter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Fronter.  If not, see <http://www.gnu.org/licenses/>.
#

extends Node

signal menu_closed
signal change_color
signal taste_the_rainbow

func _ready():
	$ColorRect.color = Color(0,0,0,1)

func _process(delta):
	if (Input.is_action_pressed("ui_quit")):
		get_tree().quit()
	#if Input.is_action_pressed("ui_accept"):
	#	_on_Button_pressed())

func _on_Resume_pressed():
	get_tree().paused = false
	emit_signal("menu_closed")
	queue_free()

func _on_Goldenrod_pressed():
	emit_signal("change_color", "daa520")

func _on_Purple_pressed():
	emit_signal("change_color", "800080")

func _on_SlateBlue_pressed():
	emit_signal("change_color", "708090")

func _on_Salmon_pressed():
	emit_signal("change_color", "e9967a")

func _on_Crimson_pressed():
	emit_signal("change_color", "dc143c")

func _on_MediumSpringGreen_pressed():
	emit_signal("change_color", "3cb371")

func _on_Tomato_pressed():
	emit_signal("change_color", "ff3e2d")
	prints("put tomato in dere")

func _on_DarkenedLawnGreen_pressed():
	emit_signal("change_color", "7bd126")

func _on_DeepSkyBlue_pressed():
	emit_signal("change_color", "00bfff")

#### NOT PAUSING GAME? ####
func _on_Rainbow_pressed():
	get_tree().paused = false
	emit_signal("taste_the_rainbow")