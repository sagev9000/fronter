#
# Copyright (C) 2018  Sage Vaillancourt, sagev9000@gmail.com
#
# This file is part of Fronter.
# 
# Fronter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Fronter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Fronter.  If not, see <http://www.gnu.org/licenses/>.
#

extends Area2D

# signals
signal update_display # tells parent to update points display
signal refund # will tell parent a refund is occuring, for display
signal shooting_speed_upgrade # signals that a shooting speed upgrade has occured
signal other_shooting_upgrade
signal other_damage_upgrade
signal double_laser_upgrade
signal other_laser_penetration_upgrade
signal other_ship_color_change
signal other_ship_enable_rainbow
signal shooting_speed_fully_upgraded
signal restart_game
signal multiplayer_movement
signal start_next_round
signal buy_turret
signal upgrade_turret_range

export (PackedScene) var Laser

################################
# THINGS THAT MAY NEED ADJUSTING
#################################

var ship_speed = 500

# first 5 tiers of shooting speed
#const BULLET_DELAY_DICT = {1:0.8, 2:0.5, 3:0.4, 4:0.2, 5:0.05} #original
const BULLET_DELAY_DICT = {1:0.8, 2:0.6, 3:0.4, 4:0.2, 5:0.1}
# starting cost of shooting speed upgrades after tier 5
const SHOOTING_SPEED_UPGRADE_DEFAULT = 1500

# can't shoot faster than this
const BULLET_DELAY_MINIMUM = 0.01

# costs of shooting speed upgrades
const BULLET_DELAY_COST = {2:100, 3:400, 4:1000, 5:4000, 6:0}

# double laser cost
const DOUBLE_LASER_COST = 6000

# 5 tiers of laser damage
const LASER_DAMAGE_DICT = {1:10, 2:15, 3:20, 4:40, 5:50}

# costs of laser damage upgrades
const LASER_DAMAGE_COST = {2:400, 3:1000, 4:2000, 5:6000, 6:0}

# costs of laser penetration upgrades
const LASER_PENETRATION_COST = {2:400, 3:1000, 4:2000, 5:6000, 6:0}

# 5 tiers of laser penetration
const LASER_PENETRATION_DICT = {1:0,2:1,3:2,4:3,5:5}
# the ship's starting position
const STARTING_POSITION = Vector2(130, 250)

# the amount of money the player starts with
#const STARTING_MONEY = 0
const STARTING_MONEY = 100000

# cost to unlock turrets
const UNLOCK_TURRETS_COST = 1000

#################################

# set sliding shooting speed upgrade costs
var shooting_speed_upgrade = SHOOTING_SPEED_UPGRADE_DEFAULT

# used to limit player movement
var screensize

# can the player shoot at this instant
var can_shoot = false

# timer for shooting speed
var timer = null

# sets the delay between shots to the default
var bullet_delay = BULLET_DELAY_DICT[1]
var bullet_delay_tier = 1

# sets other shooting settings to their defaults
var laser_penetration = LASER_PENETRATION_DICT[1]
var laser_penetration_tier = 1
var double_laser = false
var laser_damage = LASER_DAMAGE_DICT[1]
var laser_damage_tier = 1

# turret settings
var turrets_unlocked = false
var has_turret = false

# gives the player their starting points
var money = STARTING_MONEY

# sets initial value of ship, to be added to with upgrades for refunds
var ship_value = 0

# what percentage of points should be returned on refunding
var refund_percentage = 1

# is a menu open? try to ignore input in-game
# only really relevant to touchscreens
var menu_open = false

var boss = false
var damage = 0

# opens the upgrade menu
func upgradeMenu():
	var upgmenu = preload("res://UpgradeMenu.tscn").instance()
	menu_open = true
	add_child(upgmenu)
	upgmenu.connect("refund", self, "_refund_button")
	upgmenu.connect("bullet_delay_upgrade", self, "upgradeBulletDelay")
	upgmenu.connect("double_laser_upgrade", self, "doubleLaserUpgrade")
	upgmenu.connect("laser_penetration_upgrade", self, "laserPenetrationUpgrade")
	upgmenu.connect("change_color", self, "changeColor")
	upgmenu.connect("taste_the_rainbow", self, "enable_the_rainbow")
	upgmenu.connect("laser_damage_upgrade", self, "upgradeLaserDamage")
	upgmenu.connect("menu_closed", self, "upgradeMenuClosed")
	upgmenu.connect("unlock_turrets", self, "unlockTurrets")
	upgmenu.connect("buy_turret", self, "buyTurret")
	upgmenu.connect("turret_menu", self, "openTurretMenu")
	upgmenu.laser_damage_tier = laser_damage_tier
	upgmenu.bullet_delay_tier = bullet_delay_tier
	upgmenu.laser_penetration_tier = laser_penetration_tier
	upgmenu.turrets_unlocked = turrets_unlocked
	upgmenu.has_turret = has_turret
	if !get_tree().has_network_peer():
		get_tree().paused = true
		
# signalled on close of UpgradeMenu
func upgradeMenuClosed():
	menu_open = false
	emit_signal("start_next_round")

func pauseMenu():
	menu_open = true
	var pausemenu = preload("res://PauseMenu.tscn").instance()
	add_child(pausemenu)
	pausemenu.connect("change_color", self, "changeColor")
	pausemenu.connect("menu_closed", self, "pauseMenuClosed")
	pausemenu.connect("taste_the_rainbow", self, "enable_the_rainbow")
	if !get_tree().has_network_peer():
		get_tree().paused = true

func pauseMenuClosed():
	menu_open = false

func openTurretMenu():
	var turretmenu = preload("res://TurretMenu.tscn").instance()
	add_child(turretmenu)
	turretmenu.connect("buy_turret", self, "buyTurret")
	turretmenu.connect("turret_range_upgrade", self, "turretRangeUpgrade")
	turretmenu.screensize = screensize
	if !get_tree().has_network_peer():
		get_tree().paused = true

func makePurchaseFor(cost):
	if money >= cost:
		money -= cost
		emit_signal("update_display")
		return true
	else:
		return false

#######################
# REQUESTING UPGRADES #
#######################
# all will request their upgrade
# all will pause the game if not in multiplayer
# checks which tier the player is already on, and if they have enough points
# upgrades and charges the player, if possible
# increases the ship value by the amount spent, for refunds
# sends an rpc message about current state for a given upgrade

func unlockTurrets():
	if money >= UNLOCK_TURRETS_COST:# && turrets_unlocked == false:
		money -= UNLOCK_TURRETS_COST
		turrets_unlocked = true
		emit_signal("update_display")
		openTurretMenu()
		# pause if not multiplayer
		if !get_tree().has_network_peer():
			get_tree().paused = true

func buyTurret(turret_position, cost):
	#money -= cost
	emit_signal("update_display")
	if !get_tree().has_network_peer():
		emit_signal("buy_turret", get_tree().get_network_unique_id(), turret_position)
		has_turret = true
		get_tree().paused = true
	else:
		rpc("net_buying_turret", get_tree().get_network_unique_id(), turret_position)

sync func net_buying_turret(id, turret_position):
	emit_signal("buy_turret", id, turret_position)

func turretRangeUpgrade(cost, new_scale):
	if makePurchaseFor(cost):
		emit_signal("upgrade_turret_range", get_tree().get_network_unique_id(), new_scale)
		if !get_tree().has_network_peer():
				get_tree().paused = true

#################################################
# ALLOWS THE PLAYER TO SHOOT TWO LASERS AT ONCE #
func doubleLaserUpgrade():
	# if the user has enough money, take that money, and enable doubles
	if money >= DOUBLE_LASER_COST && double_laser == false:
		money -= DOUBLE_LASER_COST
		double_laser = true
		$Gunner2.visible = true
	
	# tell other players to enable doubles for you
	rpc("double_laser_upgrade", get_tree().get_network_unique_id())
	
	# tell main to update the points display
	emit_signal("update_display")
	
	# pause if not multiplayer
	if !get_tree().has_network_peer():
		get_tree().paused = true

# tell main about other ship's double_laser
remote func double_laser_upgrade(id):
	emit_signal("double_laser_upgrade", id)
# ALLOWS THE PLAYER TO SHOOT TWO LASERS AT ONCE #
#################################################


######################################################
# ALLOWS LASERS TO PENETRATE ENEMIES BEFORE EXPIRING #
var next_penetration_upgrade_cost = LASER_PENETRATION_COST[laser_penetration_tier + 1]
func laserPenetrationUpgrade():
	if laser_penetration_tier < 5:
		if money >= LASER_PENETRATION_COST[laser_penetration_tier + 1]:
			laser_penetration_tier += 1
			laser_penetration = LASER_PENETRATION_DICT[laser_penetration_tier]
			money -= LASER_PENETRATION_COST[laser_penetration_tier]
			ship_value += LASER_PENETRATION_COST[laser_penetration_tier]
			next_penetration_upgrade_cost = LASER_PENETRATION_COST[laser_penetration_tier + 1]
			print(laser_penetration)
	emit_signal("update_display")
	
	rpc("other_laser_penetration_upgrade", get_tree().get_network_unique_id(), laser_penetration)
	# pause if not multiplayer
	if !get_tree().has_network_peer():
		get_tree().paused = true


remote func other_laser_penetration_upgrade(id):
	emit_signal("other_laser_penetration_upgrade", id)

# tell main about other ship's
# ALLOWS LASERS TO PENETRATE ENEMIES BEFORE EXPIRING #
######################################################


###############################################
# INCREASES THE FIRE RATE FOR PLAYERS' LASERS #
var next_bullet_delay_upgrade_cost = BULLET_DELAY_COST[bullet_delay_tier + 1]
func upgradeBulletDelay():
	if bullet_delay_tier < 5:
		if money >= BULLET_DELAY_COST[bullet_delay_tier + 1]:
			bullet_delay_tier += 1
			bullet_delay = BULLET_DELAY_DICT[bullet_delay_tier]
			money -= BULLET_DELAY_COST[bullet_delay_tier]
			ship_value += BULLET_DELAY_COST[bullet_delay_tier]
			next_bullet_delay_upgrade_cost = BULLET_DELAY_COST[bullet_delay_tier + 1]
			print(bullet_delay)
	
	timer.set_wait_time(bullet_delay)
	rpc("other_shooting_speed_upgrade", get_tree().get_network_unique_id(), bullet_delay)
	emit_signal("update_display")
	if !get_tree().has_network_peer():
		get_tree().paused = true

# tell main about other ship's bullet_delay upgrade
remote func other_shooting_speed_upgrade(id, bullet_delay):
	emit_signal("other_shooting_upgrade", id, bullet_delay)
# INCREASES THE FIRE RATE FOR PLAYERS' LASERS #
###############################################


############################################
# INCREASES DAMAGE DONE BY EACH LASER SHOT #
var next_laser_damage_upgrade_cost = LASER_DAMAGE_COST[laser_damage_tier + 1]
func upgradeLaserDamage():
	if laser_damage_tier < 5:
		if money >= LASER_DAMAGE_COST[laser_damage_tier + 1]:
			laser_damage_tier += 1
			laser_damage = LASER_DAMAGE_DICT[laser_damage_tier]
			money -= LASER_DAMAGE_COST[laser_damage_tier]
			ship_value += LASER_DAMAGE_COST[laser_damage_tier]
			next_laser_damage_upgrade_cost = LASER_DAMAGE_COST[laser_damage_tier + 1]
			print(laser_damage)

	rpc("other_laser_damage_upgrade", get_tree().get_network_unique_id(), laser_damage)
	emit_signal("update_display")
	if !get_tree().has_network_peer():
		get_tree().paused = true

# tell main about other ship's laser_damage upgrade
remote func other_laser_damage_upgrade(id, laser_damage):
	emit_signal("other_damage_upgrade", id, laser_damage)
# INCREASES DAMAGE DONE BY EACH LASER SHOT #
############################################

func _ready():
	# for measuring time between lasers
	timer = Timer.new()
	timer.set_one_shot(true)
	timer.set_wait_time(bullet_delay)
	timer.connect("timeout", self, "on_timeout_complete")
	add_child(timer)
	timer.start()
	
	# set player's position to the default
	position = STARTING_POSITION
	
	# sets screensize to prevent player leaving screen
	screensize = get_viewport_rect().size
	
	# starts the ship's rocketing animation
	$AnimatedSprite.play()

# resets all upgrades to the default
# refunds them with adjustable percentage returned
# not currently 100%, so disabled
func _refund_button():
	bullet_delay = BULLET_DELAY_DICT[1]
	bullet_delay_tier = 1
	timer.set_wait_time(bullet_delay)
	laser_penetration = 0
	double_laser = false
	rainbow_is_on = false
	changeColor(Color(1,1,1,1))
	shooting_speed_upgrade = SHOOTING_SPEED_UPGRADE_DEFAULT
	money += ship_value*refund_percentage
	$Gunner2.visible = false
	prints("Refunded ", ship_value*refund_percentage)
	ship_value = 0
	emit_signal("update_display")
	#get_tree().paused = true

############################
# Changes the ship's color #
func changeColor(color):
	$AnimatedSprite.modulate = color
	if get_tree().has_network_peer():
		rpc("_change_color", get_tree().get_network_unique_id(), color)

# tell main about other ship's color change
remote func _change_color(id, color):
	emit_signal("other_ship_color_change", id, color)

# Changes the ship's color #
############################

func on_timeout_complete():
	can_shoot = true

var is_shooting

func moveto(finger_position):
	if !menu_open:
		if (finger_position.x > position.x && finger_position.x < 800):
			velocity.x += 1
		if position.x > finger_position.x:
			velocity.x -= 1
		if (finger_position.y - 100 > position.y && finger_position.x < 800):
			velocity.y += 1
		if (position.y > finger_position.y - 100 && finger_position.x < 800):
			velocity.y -= 1

remote func move_player(id, position, is_shooting):
	emit_signal("multiplayer_movement", id, position, is_shooting)

# the player's movement vector
var velocity = Vector2()

var rainbow = Color(0,0,0,1)
var r_up = true
var g_up = true
var b_up = true
var rainbow_speed = 30
var rainbow_is_on = false

func _process(delta):
	# move player with keyboard
	velocity = Vector2()
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
	if (velocity.length() > 0):
		velocity = velocity.normalized() * ship_speed
	position += velocity * delta
	
	if rainbow_is_on:
		rainbow(delta)
	
	# prevents player leaving the screen
	position.x = clamp(position.x-100, 0, screensize.x-164) + 100
	position.y = clamp(position.y, 0, screensize.y)
	
	# basically exists just for rpc messages about whether or not the player is currently firing
	if can_shoot:
		is_shooting = false
	else:
		is_shooting = true
	
	# if in multiplayer mode, push position and shooting status as often as possible
	#if get_tree().has_network_peer():
	#	rpc_unreliable("move_player", get_tree().get_network_unique_id(), position, is_shooting)
	
	# if shoot button is pressed, try shooting
	if Input.is_action_pressed("ui_accept"):
		shoot()
	
	# ui_reset button opens the upgrade menu
	if Input.is_action_pressed("ui_reset"):
		upgradeMenu()

# spawn lasers to shoot
# lasers can be set to penetrate or not
# can fire two lasers at once if double_laser is on
func shoot():
	if can_shoot:
		var laser = Laser.instance()
		get_node("../").add_child(laser)
		laser.current_pen = laser_penetration
		laser.position.y = position.y - 27
		laser.position.x = position.x + 46
		laser.damage = laser_damage
		
		#MAYBE THE LASERS SHOULD BE THEIR OWN NODES
		#Would allow for more simple additions in the future
		if double_laser == true:
			var laser2 = Laser.instance()
			get_node("../").add_child(laser2)
			laser2.position.y = position.y + 31
			laser2.position.x = position.x + 50
			laser2.current_pen = laser_penetration
			laser2.damage = laser_damage
			
		# don't shoot again until the timer resets 
		can_shoot = false
		timer.start()

# Funky colors #
# NOT CURRENTLY REFUNDABLE #
func enable_the_rainbow():
	if money >= 2000 && not rainbow_is_on:
		money -= 2000
		emit_signal("update_display")
		rainbow_is_on = true
		rpc("_enable_rainbow", get_tree().get_network_unique_id())
	
	if !get_tree().has_network_peer():
		get_tree().paused = true

remote func _enable_rainbow(id):
	emit_signal("other_ship_enable_rainbow", id)

func rainbow(delta):
	if rainbow.r < 1 && r_up:
		rainbow.r += rainbow_speed*delta/10
		if rainbow.r >= 1: r_up = false
	else:
		rainbow.r -= rainbow_speed*delta/10
		if rainbow.r <= 0.2: r_up = true
	
	if rainbow.g < 1 && g_up:
		rainbow.g += rainbow_speed*delta/9
		if rainbow.g >= 1: g_up = false
	else:
		rainbow.g -= rainbow_speed*delta/9
		if rainbow.g <= 0.2: g_up = true
	
	if rainbow.b < 1 && b_up:
		rainbow.b += rainbow_speed*delta/8
		if rainbow.b >= 1: b_up = false
	else:
		rainbow.b -= rainbow_speed*delta/8
		if rainbow.b <= 0.2: b_up = true
	
	$AnimatedSprite.modulate = rainbow

var gameover
# displays endgame screen and pauses
func gameOver():
	gameover = preload("res://GameOver.tscn").instance()
	add_child(gameover)
	gameover.connect("restart", self, "restart_game")
#	gameover.connect("bullet_delay_upgrade", self, "upgradeBulletDelay_button")
	get_tree().paused = true

# uses refund function to remove upgrades
# sets points back to default
func restart_game():
	_refund_button()
	get_tree().paused = false
	money = STARTING_MONEY
	emit_signal("restart_game")
	gameover.queue_free()

func _on_Timer_timeout():
	if get_tree().has_network_peer():
		rpc_unreliable("move_player", get_tree().get_network_unique_id(), position, is_shooting)
