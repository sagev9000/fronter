#
# Copyright (C) 2018  Sage Vaillancourt, sagev9000@gmail.com
#
# This file is part of Fronter.
# 
# Fronter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Fronter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Fronter.  If not, see <http://www.gnu.org/licenses/>.
#

extends Node

signal restart
var rectangle_opacity = 0

func _ready():
	if get_tree().has_network_peer() && not get_tree().is_network_server():
		$Restart.visible = false
		$Button.disabled = true

func _process(delta):
	$ColorRect.color = Color(0, 0, 0, rectangle_opacity)
	if rectangle_opacity < 1:
		rectangle_opacity += delta/2
	if (Input.is_action_pressed("ui_quit")):
		get_tree().quit()


func _on_Button_pressed():
	emit_signal("restart")
	#queue_free()
	
func _on_Button_button_down():
	$Restart.frame = 1
