#
# Copyright (C) 2018  Sage Vaillancourt, sagev9000@gmail.com
#
# This file is part of Fronter.
#
# Fronter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Fronter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Fronter.  If not, see <http://www.gnu.org/licenses/>.
#

extends Node

signal menu_closed
signal change_color
signal buy_turret
signal turret_range_upgrade
signal turret_damage_upgrade
signal turret_speed_upgrade

var screensize

const DEFAULT_TURRET_COST = 0#1000

const TIER_COST = {2:1000, 3:4000, 4:10000, 5:0}

func _ready():
	$ColorRect.color = Color(0,0,0,.8)

	if get_parent().has_method("upgradeTurret"):
		$BuyTurret.visible = false

	$Back.visible = false
	$BuyTurret.visible = false
	$Accept.visible = true
	if get_parent().has_method("upgradeTurret") != true:
		pass
	else:
		$Upgrade/Button.text = str("Upgrade: $", TIER_COST[get_parent().tier+1])
		if get_parent().tier >= 4:
			$Upgrade/Button.visible = false

func _input(event):
	pass

var velocity = Vector2()
func _process(delta):
	if (Input.is_action_pressed("ui_quit")):
		get_tree().quit()
	#if Input.is_action_pressed("ui_accept"):
	#	_on_Button_pressed()

	if get_parent().has_method("upgradeTurret") == true:
		$Upgrade.visible = true

func _on_Button_pressed():
	queue_free()

func backButton():
	queue_free()

#var newTurret
#func buyTurret():
#	if get_parent().money >= DEFAULT_TURRET_COST:
		#var turret = preload("res://Turret.tscn").instance()
		#turret.set_name(turret.get_name())
		#newTurret = turret.get_name()
		#pass

func acceptTurret():
	if get_parent().has_method("upgradeTurret") == true:
		queue_free()
	else:
		get_tree().paused = false
		#get_parent().has_turret = true
		$Accept.visible = false
		$Back.visible = true
	queue_free()

func upgradeTurret():
	get_tree().paused = false
	if get_parent().get_parent().get_node("Player").makePurchaseFor(TIER_COST[get_parent().tier+1]):
		get_parent().upgradeTurret()
		get_parent().relayUpgrade()
		if get_parent().tier >= 4:
			$Upgrade/Button.visible = false
		else:
			$Upgrade/Button.text = str("Upgrade: $", TIER_COST[get_parent().tier+1])
