extends "res://Bad.gd"

func _ready():
	health = 500 #500
	speed = 50
	pass

signal health_up

var centered_x = false
var centered_y = false

func _process(delta):
	if health < 200:
		speed = 0
		$StarSprite.frame = 1
		$MainSprite.visible = false
		$CollisionBox.disabled = true
		
		if centered_x && centered_y:
			$StarSprite.rotation += delta*10
			position.x -= delta*500
			if position.x < -50:
				emit_signal("health_up", 200)
				queue_free()
		else:
			if position.x < 500:
				position.x += delta*(544-position.x)+1
			elif position.x > 524:
				position.x -= delta*(position.x-480)+1
			else:
				centered_x = true
			
			if position.y < 280:
				position.y += delta*(320-position.y)+1
			elif position.y > 320:
				position.y -= delta*(position.y-280)+1
			else:
				centered_y = true