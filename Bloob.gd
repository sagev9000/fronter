#
# Copyright (C) 2018  Sage Vaillancourt, sagev9000@gmail.com
#
# This file is part of Fronter.
# 
# Fronter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Fronter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Fronter.  If not, see <http://www.gnu.org/licenses/>.
#

extends Area2D

signal dead
export (int) var SPEED
var screensize
var health_multi = 1

var boss = false
var health = 150
var hit_timer = 1000

func _ready():
	connect("area_entered", self, "hit")
	pass
	
func _on_Visibility_screen_exited():
    queue_free()

func hit(who):
	health -= who.damage/health_multi
#	$AnimatedSprite.frame = 1
	hit_timer = 0
	
var velocity = Vector2()

func _process(delta):
	if health <= 0:
		emit_signal("dead", 25) # was 40 # was 50
		queue_free()
	if position.x < -100:
		emit_signal("dead", 0)
		queue_free()
	
	if hit_timer < 0.15:
		hit_timer += delta
	elif hit_timer < 0.25:
		hit_timer += delta
	else:
		velocity.x -= 1
#		$AnimatedSprite.frame = 0
	 # the player's movement vector

	if velocity.length() > 0:
		velocity = velocity.normalized() * SPEED
	
	var health_bar = Vector2(((health * 2.5) - 157), -225)
	$Line2D.set_point_position( 1, health_bar )
	
	position += velocity * delta