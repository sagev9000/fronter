#
# Copyright (C) 2018  Sage Vaillancourt, sagev9000@gmail.com
#
# This file is part of Fronter.
# 
# Fronter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Fronter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Fronter.  If not, see <http://www.gnu.org/licenses/>.
#

extends Area2D

slave var slave_health

signal game_over
export (PackedScene) var Laser
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

const STARTING_HEALTH = 500

var health = STARTING_HEALTH
var double_laser = false
var can_shoot = true
var timer = null
var bullet_delay = 0.1
var damage = 0

func _ready():
	_update_health_bar()

func _process(delta):
	if get_tree().has_network_peer() && not get_tree().is_network_server():
		#health = slave_health
		pass

	if $Line2D.default_color.r > 0:
		$Line2D.default_color.r -= delta
	if $Line2D.default_color.g < 180:
		$Line2D.default_color.g += delta
	if $Line2D.default_color.b > 0:
		$Line2D.default_color.b -= delta

	if health <= 100 && $BlownOut/WhoopsGuy.unit_offset < 1:
		$BlownOut/WhoopsGuy.unit_offset += delta

func _on_Mothership_area_shape_entered(area_id, area, area_shape, self_shape):
	#if not get_tree().has_network_peer():
	if get_tree().is_network_server() == false:
		health -= 50
	if get_tree().is_network_server() == true:
		health -= 50
		rset("slave_health", health)
	
	$Line2D.default_color = Color("b70000")
	
	if area.boss:
			emit_signal("flash")
			health = 0
	
	_update_health_bar()
	
	if health <= 250:
		$BaseSprite.frame = 1
	if health <= 100:
		$BaseSprite.frame = 2
	if health <= 0:
		$BaseSprite.frame = 3
		emit_signal("game_over")
		prints("Game Over!")

func _on_Player_restart_game():
	health = STARTING_HEALTH
	$BaseSprite.frame = 0
	var health_bar = Vector2(((health * (.8)) + 100), 300)
	$Line2D.set_point_position( 1, health_bar )

func _update_health_bar():
	var health_bar = Vector2(((health * (.8)) + 100), 300)
	$Line2D.set_point_position( 1, health_bar )