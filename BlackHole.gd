extends "res://Bad.gd"

var rot_speed = 1
var dying = false
var splitting = false

signal flash

func _ready():
	starting_health = 5000
	health = starting_health
	speed = 10
	kill_reward = 1000
	boss = true

func _process(delta):
	rotation += delta*rot_speed
	if rotation > 2*PI:
		rotation = 0
	
	if dying == true:
		$TopHalf.visible = true
		$BotHalf.visible = true
		$MainSprite.visible = false
		if rotation_degrees > 45 && rotation_degrees < 70:
			if rot_speed != 0:
				emit_signal("flash")
			splitting = true
		health = 999999
	elif health < starting_health*0.16:
		dying = true
		#$CollisionBox.disabled = true
	elif health < starting_health*0.28:
		if $MainSprite.frame != 6:
			emit_signal("flash")
			$MainSprite.frame = 6
	elif health < starting_health*0.40:
		if $MainSprite.frame != 5:
			emit_signal("flash")
			$MainSprite.frame = 5
	elif health < starting_health*0.52:
		if $MainSprite.frame != 4:
			emit_signal("flash")
			$MainSprite.frame = 4
	elif health < starting_health*0.64:
		if $MainSprite.frame != 3:
			emit_signal("flash")
			$MainSprite.frame = 3
	elif health < starting_health*0.76:
		if $MainSprite.frame != 2:
			emit_signal("flash")
			$MainSprite.frame = 2
	elif health < starting_health*0.88:
		if $MainSprite.frame != 1:
			emit_signal("flash")
			$MainSprite.frame = 1
	
	if splitting:
		$CollisionBox.disabled = true
		rot_speed = 0
		$TopHalf.position.y -= delta*500
		$TopHalf.position.x -= delta*250
		$BotHalf.position.y += delta*500
		$BotHalf.position.x += delta*250
		
		if $TopHalf.position.y < -2000:
			$Music.volume_db -= delta*50
		
		if $TopHalf.position.y < -4000:
			emit_signal("dead", kill_reward)
			queue_free()
		
		$TopHalf.rotation += delta
		$BotHalf.rotation -= delta