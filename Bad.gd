#
# Copyright (C) 2018  Sage Vaillancourt, sagev9000@gmail.com
#
# This file is part of Fronter.
# 
# Fronter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Fronter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Fronter.  If not, see <http://www.gnu.org/licenses/>.
#

extends Area2D

signal dead
var speed = 50
var screensize
var health_multi = 1
var kill_reward = 0

var boss = false
var health = 1
var starting_health = 1
var hit_timer = 1000

func _on_Visibility_screen_exited():
	queue_free()

func _ready():
	connect("area_entered", self, "hit")
	pass

func hit(who):
	health -= who.damage/health_multi
	hit_timer = 0
	
	#var health_bar = Vector2(((health * 6) - 157), -273)
	#$HealthBar.set_point_position( 1, health_bar )
	
	if health <= 0:
		emit_signal("dead", kill_reward)
		queue_free()

var velocity = Vector2()

func _process(delta):
	velocity.x -= 1
	
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
	
	if position.x < -100:
		emit_signal("dead", 0)
		queue_free()
	
	position += velocity * delta