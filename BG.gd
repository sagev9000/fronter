extends Node2D
var fly_speed = 5


func _ready():
	pass

func _process(delta):
	$bg1.position.x -= delta*fly_speed
	$bg2.position.x -= delta*fly_speed
	if $bg1.position.x < -550:
		$bg1.position.x = 1600
	if $bg2.position.x < -550:
		$bg2.position.x = 1600
	
	if $Sun.visible == true:
		if $Sun.position.x < -2500:
			$Sun.visible = false
			$Sun.position.x = 1250
		$Sun.position.x -= delta*2.5