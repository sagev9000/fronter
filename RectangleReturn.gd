extends "res://Bad.gd"

signal flash
signal boss_health

var right_laser_alive = true
var left_laser_alive = true

var move_left = true
var move_down = true

var speed_multiplier = 3

var fight_started = false

func _ready():
	speed = 0
	position.x = 1200
	starting_health = 2800
	health = starting_health
	$RightArm/BadLaser.independent = false
	$RightArm/BadLaser.health_multi = 5
	$LeftArm/BadLaser.independent = false
	$LeftArm/BadLaser.health_multi = 5
	$RightArm/BadLaser.fire_speed = 0
	$LeftArm/BadLaser.fire_speed = 0
	$RightArm/BadLaser/Rotatable.rotation_degrees = -45
	
#	$RightArm/BadLaser.health = 10
#	$LeftArm/BadLaser.health = 10
	pass

func _process(delta):
	#$RightArm/Forearm.rotate(delta/10)
#	print(health)
	wobble(delta)
#	print(position.x)

	if position.x < 903:
		move_left = false
#		health -= 25*delta
		if !fight_started:
			$LeftArm/AnimationPlayer.play("block")
			$RightArm/AnimationPlayer.play("block")
			fight_started = true
	
	if move_left:
		position.x -= delta*20
	
	if $LeftArm/AnimationPlayer.assigned_animation == "retract" && $LeftArm/AnimationPlayer.current_animation != "retract":
		$RightArm/BadLaser.fire_speed = 50
		$LeftArm/BadLaser.fire_speed = 50
	
	if health <= 2000:
		if $LeftArm/AnimationPlayer.assigned_animation != "blockL" && $LeftArm/AnimationPlayer.assigned_animation != "spinoff":
			$LeftArm/AnimationPlayer.play("blockL")
		if $RightArm/AnimationPlayer.assigned_animation != "blockL" && $RightArm/AnimationPlayer.assigned_animation != "spinoff":
			$RightArm/AnimationPlayer.play("blockL")
			$RightArm/AnimationPlayer.seek(0.5)
	if health <= 2500 && health > 2000:
		if $LeftArm/AnimationPlayer.assigned_animation != "retract":
			$LeftArm/AnimationPlayer.play("retract")
		if $RightArm/AnimationPlayer.assigned_animation != "retract":
			$RightArm/AnimationPlayer.play("retract")
	
#	health -= delta*100
	
	if right_laser_alive:
		if $RightArm/BadLaser:
#			$RightArm/BadLaser.health -= delta*30
			if $RightArm/BadLaser.health <= 100:
				$RightArm/BadLaser.fire_speed = 150
	elif $RightArm/BadLaser/AnimatedSprite.frame != 1:
		$RightArm/AnimationPlayer.play("spinoff")
		$RightArm/BadLaser/AnimatedSprite.frame = 1
		$RightArm/BadLaser.fire_speed = 0
		$RightArm/BadLaser/Line2D.visible = false
	
	if left_laser_alive:
		if $LeftArm/BadLaser:
#			$LeftArm/BadLaser.health -= delta*30
			if $LeftArm/BadLaser.health <= 100:
				$LeftArm/BadLaser.fire_speed = 150
	elif $LeftArm/BadLaser/AnimatedSprite.frame != 1:
		$LeftArm/AnimationPlayer.play("spinoff")
		$LeftArm/BadLaser.fire_speed = 0
		$LeftArm/BadLaser/AnimatedSprite.frame = 1
		$LeftArm/BadLaser/Line2D.visible = false

func hit(who):
	if fight_started:
		health -= who.damage/(health_multi*3.5)
		print("Rectangle Return: ", health)
		if get_tree().is_network_server():
				rpc("bossHealth", health)
		emit_signal("boss_health", health, starting_health)

func _on_LeftBadLaser_dead(money):
	if left_laser_alive:
		left_laser_alive = false

func _on_RightBadLaser_dead(money):
	if right_laser_alive:
		right_laser_alive = false

func normalFireSpeed():
#	$RightArm/BadLaser.fire_speed = 25
#	$LeftArm/BadLaser.fire_speed = 25
	$RightArm/BadLaser.fire_speed = 10
	$LeftArm/BadLaser.fire_speed = 10
	if get_tree().is_network_server():
			rpc("bossHealth", health)
	emit_signal("boss_health", health, starting_health)

func wobble(delta):
	if position.y - 305 > 4:
		move_down = false
	if position.y - 305 <= -4:
		move_down = true
	
	if abs(position.y - 305) > 4:
		speed_multiplier = 1
	elif abs(position.y - 305) > 3:
		speed_multiplier = 1
	elif abs(position.y - 305) > 2:
		speed_multiplier = 2
	elif abs(position.y - 305) <= 2:
		speed_multiplier = 3
	
	if move_down:
		if health > 1000:
			position.y += delta*speed_multiplier
	else:
		if health > 1000:
			position.y -= delta*speed_multiplier

func flash():
	emit_signal("flash")

func _exit_tree():
	$LeftArm/BadLaser.queue_free()
	$RightArm/BadLaser.queue_free()