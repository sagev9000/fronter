#
# Copyright (C) 2018  Sage Vaillancourt, sagev9000@gmail.com
#
# This file is part of Fronter.
# 
# Fronter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Fronter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Fronter.  If not, see <http://www.gnu.org/licenses/>.
#

extends Node

signal refund
signal speed_upgrade
signal bullet_delay_upgrade
signal laser_damage_upgrade
signal double_laser_upgrade
signal laser_penetration_upgrade
signal add_laser
signal plasma_lasers
signal menu_closed
signal change_color
signal taste_the_rainbow
signal unlock_turrets
signal buy_turret
signal turret_menu

const DEFAULT_TURRET_COST = 1000
var turret_cost = DEFAULT_TURRET_COST

var laser_damage_tier
var bullet_delay_tier
var shooting_speed_tier
var laser_penetration_tier
var double_lasers
var turrets_unlocked = false
var has_turret

func _ready():
	get_tree().paused = true
	bullet_delay_tier = get_parent().bullet_delay_tier
	$ShootingSpeedUpgrade/Icon.frame = bullet_delay_tier
	
	if get_parent().next_laser_damage_upgrade_cost == 0:
		$LaserDamageUpgrade/Cost.text = str("Sold Out!")
	else:
		$LaserDamageUpgrade/Cost.text = str("$", get_parent().next_laser_damage_upgrade_cost)
	
	if get_parent().next_bullet_delay_upgrade_cost == 0:
		$ShootingSpeedUpgrade/Cost.text = str("Sold Out!")
	else:
		$ShootingSpeedUpgrade/Cost.text = str("$", get_parent().next_bullet_delay_upgrade_cost)
	
	if get_parent().next_penetration_upgrade_cost == 0:
		$LaserPenetrationUpgrade/Cost.text = str("Sold Out!")
	else:
		$LaserPenetrationUpgrade/Cost.text = str("$", get_parent().next_penetration_upgrade_cost)
	
	$ColorRect.color = Color(0,0,0,1)

func _process(delta):
	$ShootingSpeedUpgrade/Icon.frame = bullet_delay_tier-1
	$LaserDamageUpgrade/Icon.frame = laser_damage_tier-1
	$LaserPenetrationUpgrade/Icon.frame = laser_penetration_tier-1
	
	if (Input.is_action_pressed("ui_quit")):
		get_tree().quit()
	#if Input.is_action_pressed("ui_accept"):
	#	_on_Button_pressed()

func _on_Button_pressed():
	get_tree().paused = false
	emit_signal("menu_closed")
	queue_free()

func _on_Refund_pressed():
	bullet_delay_tier = 0
	$ShootingSpeedUpgrade/Icon.frame = 0
	get_tree().paused = false
	emit_signal("refund")

func _on_ShootingSpeedUpgrade_pressed():
	get_tree().paused = false
	emit_signal("bullet_delay_upgrade")
	bullet_delay_tier = get_parent().bullet_delay_tier
	if get_parent().next_bullet_delay_upgrade_cost == 0:
		$ShootingSpeedUpgrade/Cost.text = str("Sold Out!")
	else:
		$ShootingSpeedUpgrade/Cost.text = str("$", get_parent().next_bullet_delay_upgrade_cost)

func _on_DoubleLaserUpgrade_pressed():
	get_tree().paused = false
	emit_signal("double_laser_upgrade")
	prints("UPGRADE SHIP SPEED SIGNAL EMITTED")

func _on_Goldenrod_pressed():
	emit_signal("change_color", "daa520")

func _on_Purple_pressed():
	emit_signal("change_color", "800080")

func _on_SlateBlue_pressed():
	emit_signal("change_color", "708090")

func _on_Salmon_pressed():
	emit_signal("change_color", "e9967a")

func _on_Crimson_pressed():
	emit_signal("change_color", "dc143c")

func _on_MediumSpringGreen_pressed():
	emit_signal("change_color", "3cb371")

func _on_Tomato_pressed():
	emit_signal("change_color", "ff3e2d")

func _on_DarkenedLawnGreen_pressed():
	emit_signal("change_color", "7bd126")

func _on_DeepSkyBlue_pressed():
	emit_signal("change_color", "00bfff")

func _on_Rainbow_pressed():
	get_tree().paused = false
	emit_signal("taste_the_rainbow")

func _on_Laser_Damage_Upgrade():
	get_tree().paused = false
	emit_signal("laser_damage_upgrade")
	laser_damage_tier = get_parent().laser_damage_tier
	if get_parent().next_laser_damage_upgrade_cost == 0:
		$LaserDamageUpgrade/Cost.text = str("Sold Out!")
	else:
		$LaserDamageUpgrade/Cost.text = str("$", get_parent().next_laser_damage_upgrade_cost)

func _on_Laser_Penetration_Upgrade():
	get_tree().paused = false
	emit_signal("laser_penetration_upgrade")
	laser_penetration_tier = get_parent().laser_penetration_tier
	$LaserPenetrationUpgrade/Icon.frame = laser_penetration_tier
	if get_parent().next_penetration_upgrade_cost == 0:
		$LaserPenetrationUpgrade/Cost.text = str("Sold Out!")
	else:
		$LaserPenetrationUpgrade/Cost.text = str("$", get_parent().next_penetration_upgrade_cost)

func openTurretMenu():
	emit_signal("turret_menu")

func _on_TurretsButton_pressed():
	get_tree().paused = false
	emit_signal("unlock_turrets")
	turrets_unlocked = get_parent().turrets_unlocked 
	"""
	if get_parent().money >= turret_cost:
		var turret = preload("res://Turret.tscn").instance()
		turret.set_name(turret.get_name())
		newTurret = turret.get_name() 
	"""
