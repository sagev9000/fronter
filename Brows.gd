extends Node2D

var speed_multiplier = 3
var move_down = true
var start
var right_start
var left_start

func _ready():
	start = position.y
	right_start = $Right.position.x
	left_start = $Left.position.x

func _process(delta):
	wobble(delta)


func wobble(delta):
	if position.y - start > 2:
		move_down = false
	if position.y - start <= -2:
		move_down = true
	
	if abs(position.y - start) > 2:
		speed_multiplier = 1
	elif abs(position.y - start) > 1.5:
		speed_multiplier = 1
	elif abs(position.y - start) > 1:
		speed_multiplier = 2
	elif abs(position.y - start) <= 1:
		speed_multiplier = 3
	
	if move_down:
		position.y += delta*speed_multiplier
	else:
		position.y -= delta*speed_multiplier