#
# Copyright (C) 2018  Sage Vaillancourt, sagev9000@gmail.com
#
# This file is part of Fronter.
# 
# Fronter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Fronter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Fronter.  If not, see <http://www.gnu.org/licenses/>.
#

extends Area2D

signal hit

const DEFAULT_DAMAGE = 10
const DEFAULT_FALLOFF = 1.2

export (int) var SPEED
export (int) var DAMAGE
var plasma = false
var current_pen = 0
var friendly_laser = true
var damage = DEFAULT_DAMAGE
var boss = false
var falloff = DEFAULT_FALLOFF

func _ready():
	$AnimatedSprite.play()
	connect("area_entered", self, "hit")
	pass

func hit(who):
	if plasma == true:
		pass
	elif current_pen > 0:
		current_pen -= 1
		modulate=Color("ca4747")
		damage = damage/falloff
	else:
		hide()
		queue_free()

func _process(delta):
	var velocity = Vector2()
	if friendly_laser == true:
		velocity.x += 1
	else:
		velocity.x -= 1
	velocity = velocity.normalized() * SPEED
	position += velocity * delta
	
	if position.x > 2000:
		queue_free()

func _on_Laser_body_entered(body):
	emit_signal("hit")
	if plasma == false:
		$CollisionShape2D.disabled = true
		queue_free()
	else:
		var timer = null
		timer = Timer.new()
		timer.set_one_shot(true)
		timer.set_wait_time(0.1)
		timer.connect("timeout", self, "on_timeout_complete")
		add_child(timer)
		$CollisionShape2D.disabled = true

func on_timeout_complete():
	$CollisionShape2D.disabled = false