#
# Copyright (C) 2018  Sage Vaillancourt, sagev9000@gmail.com
#
# This file is part of Fronter.
# 
# Fronter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Fronter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Fronter.  If not, see <http://www.gnu.org/licenses/>.
#

extends Area2D

signal dead
export (int) var SPEED
export (PackedScene) var Laser
var screensize
var fire_speed = 15#0
var health = 400
var hit_timer = 1000
var can_shoot = true
var health_multi = 1
var boss = false 
var independent = true
func _on_Visibility_screen_exited():
	prints("bye-bye, bad bad")
	queue_free()

func _ready():
	connect("area_entered", self, "hit")
	$Rotatable.rotation_degrees = -90
	


func hit(who):
	health -= who.damage/health_multi
	#$AnimatedSprite.frame = 1
	hit_timer = 0

var velocity = Vector2()

func _process(delta):
	if health <= 0:
		emit_signal("dead", 100)
		if independent:
			queue_free()
	
	if position.x < -100 && independent:
		emit_signal("dead", 0)
		queue_free()

	$Rotatable.rotation_degrees += delta*fire_speed

	if ($Rotatable.rotation_degrees > 45 && $Rotatable.rotation_degrees < 55 && can_shoot == true):
		can_shoot = false
		shoot()

	elif ($Rotatable.rotation_degrees > 135 && $Rotatable.rotation_degrees < 145 && can_shoot == true):
		can_shoot = false
		shoot()

	elif ($Rotatable.rotation_degrees > 225 && $Rotatable.rotation_degrees < 235 && can_shoot == true):
		can_shoot = false
		shoot()

	elif ($Rotatable.rotation_degrees > 315 && $Rotatable.rotation_degrees < 325 && can_shoot == true):
		can_shoot = false
		shoot()

	elif ( $Rotatable.rotation_degrees > 55 && $Rotatable.rotation_degrees < 60):
		can_shoot = true
	elif ( $Rotatable.rotation_degrees > 145 && $Rotatable.rotation_degrees < 150):
		can_shoot = true
	elif ( $Rotatable.rotation_degrees > 235 && $Rotatable.rotation_degrees < 240):
		can_shoot = true
	elif ( $Rotatable.rotation_degrees > 325 && $Rotatable.rotation_degrees < 330):
		can_shoot = true

	if $Rotatable.rotation_degrees > 360:
		$Rotatable.rotation_degrees = 0
		can_shoot = true


	if hit_timer < 0.15:
		hit_timer += delta
		if health < 100:
			fire_speed = 150
	elif hit_timer < 0.25:
		hit_timer += delta
		velocity.x -= 1
	else:
		velocity.x -= 1
		#$AnimatedSprite.frame = 0

	if velocity.length() > 0:
		velocity = velocity.normalized() * SPEED
	
	if independent == true:
		#print(independent)
		position += velocity * delta
		var health_bar = Vector2(((health * 3.75) - 750), -1100)
		$Line2D.set_point_position( 1, health_bar )
	else:
		$Line2D.visible = false
		health = 99999

func shoot():
	if $AnimatedSprite.frame != 1:
		var laser = Laser.instance()
		get_node("../").add_child(laser)
		laser.friendly_laser = false
		laser.rotation_degrees = 180
		laser.position.y = position.y - 5
		laser.position.x = position.x - 95